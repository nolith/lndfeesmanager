package strategies

import (
	"fmt"

	"github.com/lightningnetwork/lnd/lnrpc"
)

type Applicable interface {
	Apply(c *lnrpc.Channel, local, remote *lnrpc.RoutingPolicy) *Policy
}

type Factory struct {
	DefaultStrategy Applicable

	strategies map[uint64]Applicable
}

func (s *Factory) Add(chanid uint64, strategy Applicable) error {
	if _, present := s.strategies[chanid]; present == true {
		return fmt.Errorf("Chan %d already defined a strategy", chanid)
	}

	if s.strategies == nil {
		s.strategies = make(map[uint64]Applicable)
	}

	s.strategies[chanid] = strategy

	return nil
}

func (s *Factory) Get(chanid uint64) Applicable {
	if strategy, ok := s.strategies[chanid]; ok == true {
		return strategy
	}

	return s.DefaultStrategy
}
