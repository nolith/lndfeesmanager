package strategies

import "github.com/lightningnetwork/lnd/lnrpc"

type Balance struct {
	LocalUnbalancedLimit  int64
	RemoteUnbalancedLimit int64

	LocalUnbalancedFee  Policy
	RemoteUnbalancedFee Policy
	BalancedFee         Policy
}

func localbalancePercentage(c *lnrpc.Channel) int64 {
	return c.LocalBalance * 100 / (c.Capacity - c.LocalChanReserveSat - c.RemoteChanReserveSat)
}

func (b *Balance) Apply(c *lnrpc.Channel, local, _ *lnrpc.RoutingPolicy) *Policy {
	localPercentage := localbalancePercentage(c)

	var policy Policy

	if localPercentage >= b.LocalUnbalancedLimit {
		policy = b.LocalUnbalancedFee
	} else if localPercentage <= b.RemoteUnbalancedLimit {
		policy = b.RemoteUnbalancedFee
	} else {
		policy = b.BalancedFee
	}

	changed := local.FeeBaseMsat != policy.BaseFeeMsat ||
		local.FeeRateMilliMsat != int64(policy.FeeRate*1000000) ||
		local.TimeLockDelta != policy.TimeLockDelta

	if !changed {
		return nil
	}

	return &policy
}
