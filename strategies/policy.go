package strategies

type Policy struct {
	BaseFeeMsat   int64
	FeeRate       float64
	TimeLockDelta uint32
}
